FROM debian:stretch
RUN apt-get update
RUN apt-get install -y bitlbee-libpurple git build-essential libpurple-dev

RUN git clone https://github.com/dylex/slack-libpurple.git
WORKDIR /slack-libpurple
RUN make install

CMD ["bitlbee", "-D", "-n"]
